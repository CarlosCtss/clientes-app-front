import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService} from './cliente.service';
import swal from 'sweetalert';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];

  constructor(private clienteService:ClienteService) { }

  ngOnInit(): void { 
    this.clienteService.getClientes().subscribe(
      clientes => this.clientes=clientes
    );
  }

  delete(cliente:Cliente):void{
    swal({
      title: "Estas seguro?",
      text: `¿Seguro que deseas eliminar el cliente ${cliente.nombre} ${cliente.apellido} ?`,
      icon: "warning",
      buttons: ["SI" ,"NO"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (!willDelete) {
        this.clienteService.delete(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli=>cli!==cliente);
            swal('Cliente eliminado',`Cliente ${cliente.nombre} ${cliente.apellido} eliminado con exito`);
          }
        );
      }
    });
  }

}
