import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router,ActivatedRoute } from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  public cliente:Cliente = new Cliente();
  public titulo:string = "Crear cliente";

  constructor(private clienteService : ClienteService,private router : Router, private activatedRouter:ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarCliente();
  }

  public cargarCliente():void{
    this.activatedRouter.params.subscribe(params => {
      let id = params['id'];
      if(id){
        this.clienteService.getCliente(id).subscribe( (cliente) => this.cliente = cliente );
      }
    })
  }

  public create():void{
    console.log("Clicked");
    console.log(this.cliente);
    this.clienteService.create(this.cliente)
      .subscribe(cliente => {
        this.router.navigate(['/clientes']);
        swal('Nuevo cliente',`Cliente ${cliente.nombre} creado con éxito!`,'success');
      } 
    );
  }

  public update():void{
    this.clienteService.update(this.cliente)
    .subscribe( cliente => {
      this.router.navigate(['/clientes']);
      swal('Cliente Actualizado',`Cliente ${cliente.nombre} actualizado con éxito!`);
    }
    );
  }

}
